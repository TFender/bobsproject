import {Component, OnInit} from '@angular/core';
import {Enemies} from '../DTO/Enemies';
import {Classes} from '../DTO/Classes';

@Component({
    selector: 'main-page',
    templateUrl: './mainpage.component.html',
    styleUrls: ['./mainpage.component.scss'],
})
export class MainpageComponent implements OnInit {

    public Defence: Enemies = null;
    public Offence: Classes = null;

    constructor() {
    }

    ngOnInit() {
    }


    setDefence(event: Enemies) {
      this.Defence = event;
    }

    setOffence(event: Classes) {
      this.Offence = event;
    }
}

export enum Enemies {
    ORC = 'Orc',
    CASTLE = 'Castle',
    WENCH = 'Wench',
    GLOBAL_WARMING = 'Global Warming'
}

export enum Classes {
    WIZARD = 'Wizard',
    WARRIOR = 'Warrior',
    MERCHANT = 'Merchant',
    PLUMBER = 'Plumber',
    LIZARDMAN = 'Lizardman'
}

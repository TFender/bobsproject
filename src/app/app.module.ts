import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {IonicModule} from '@ionic/angular';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppComponent} from './app.component';
import {MainpageComponent} from './mainpage/mainpage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {SidemenuComponent} from './sidemenu/sidemenu.component';
import {StatusblockComponent} from './statusblock/statusblock.component';


@NgModule({
    declarations: [
        AppComponent,
        MainpageComponent,
        SidemenuComponent,
        StatusblockComponent
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(),
        BrowserAnimationsModule
    ],
    providers: [
        StatusBar,
        SplashScreen,
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}

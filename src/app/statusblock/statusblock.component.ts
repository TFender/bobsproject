import {Component, Input, OnInit} from '@angular/core';

@Component({
    selector: 'status-block',
    templateUrl: './statusblock.component.html',
    styleUrls: ['./statusblock.component.scss'],
})
export class StatusblockComponent implements OnInit {

    public health: number = 30;
    public attack: number = 35;

    @Input() type: any;

    public increaseHealth(): void {
        this.health++;
    }

    public decreaseHealth(): void {
        this.health--;
    }

    public increaseAttack(): void {
        this.attack++;
    }

    public decreaseAttack(): void {
        this.attack--;
    }

    constructor() {
    }

    ngOnInit() {
    }

}

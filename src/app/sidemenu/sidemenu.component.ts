import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Classes} from '../DTO/Classes';
import {Enemies} from '../DTO/Enemies';

@Component({
    selector: 'side-menu',
    templateUrl: './sidemenu.component.html',
    styleUrls: ['./sidemenu.component.scss'],
})
export class SidemenuComponent implements OnInit {

    public menuActive: boolean;

    public offenceList: any[] = [];
    public defenceList: any[] = [];

    @Output() emitOffence: EventEmitter<Classes> = new EventEmitter<Classes>();
    @Output() emitDefence: EventEmitter<Enemies> = new EventEmitter<Enemies>();

    constructor() {
    }

    ngOnInit() {
        this.menuActive = false;

        this.offenceList.push({Type: Classes.WIZARD});
        this.offenceList.push({Type: Classes.WARRIOR});
        this.offenceList.push({Type: Classes.LIZARDMAN});
        this.offenceList.push({Type: Classes.MERCHANT});
        this.offenceList.push({Type: Classes.PLUMBER});

        this.defenceList.push({Type: Enemies.CASTLE});
        this.defenceList.push({Type: Enemies.GLOBAL_WARMING});
        this.defenceList.push({Type: Enemies.WENCH});
        this.defenceList.push({Type: Enemies.ORC});
    }

    public menuToggle(): void {
        console.log('klik!!!!');
        this.menuActive = !this.menuActive;
    }

    public selectOffence(offence): void {
        this.emitOffence.emit(offence);
    }

    public selectDefence(defence): void {
        this.emitDefence.emit(defence);
    }


}
